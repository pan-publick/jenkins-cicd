
resource "aws_dynamodb_table" "tf_lock_state" {
  name = "ajax-dynamo-tflock"
  billing_mode = "PAY_PER_REQUEST"
  hash_key = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }

  tags = {
    Name      = "dynamo-tflock"
    Terraform = "true"
  }
}

 terraform {
   backend "s3" {
     bucket         = "ajax-terraform-state"
     key            = "dev/terraform.tfstate"
     region         = "us-east-1"
     encrypt        = true
     dynamodb_table = "ajax-dynamo-tflock"
   }
 }
